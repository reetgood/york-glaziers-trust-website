<?php
namespace Deployer;
require 'recipe/common.php';

// Configuration

set('composer_options', '{{composer_action}} --verbose --prefer-dist --no-progress --no-interaction --no-dev --optimize-autoloader');

set('ssh_type', 'native');
set('ssh_multiplexing', true);

set('repository', 'git@bitbucket.org:reetgood/york-glaziers-trust-website.git');

set('shared_dirs', ['build/assets']);
set('writable_dirs', ['build/assets']);

// Servers

host('new.yorkglazierstrust.org')
    ->stage('production')
    ->user('james')
    ->set('deploy_path', '/var/www/html');


// Tasks

task('deploy:vendors', function () {
    if (!commandExist('unzip')) {
        writeln('<comment>To speed up composer installation setup "unzip" command with PHP zip extension https://goo.gl/sxzFcD</comment>');
    }
    run('cd {{release_path}}/build && {{bin/composer}} {{composer_options}}');
});

task('silverstripe:buildflush', function () {
   return run('{{bin/php}} {{release_path}}/build/framework/cli-script.php /dev/build flush=all');
})->desc('Run /dev/build?flush=all');

task('npm:install', function () {
    if (has('previous_release')) {
        if (test('[ -d {{previous_release}}/node_modules ]')) {
            run('cp -R {{previous_release}}/node_modules {{release_path}}');
        }
    }
    run('cd {{release_path}} && npm install');
});

task('bower:install', function () {
   run('cd {{release_path}} && bower install');
})->desc('Run bower install');

task('gulp:build', function() {
    run('cd {{release_path}} && ./node_modules/.bin/gulp build --no-lint');
})->desc('Run gulp build');

task('deploy', [
   'deploy:prepare',
   'deploy:lock',
   'deploy:release',
   'deploy:update_code',
   'deploy:vendors',
   'deploy:shared',
   'deploy:writable',
   'npm:install',
   'bower:install',
   'gulp:build',
   'silverstripe:buildflush',
   'deploy:symlink',
   'deploy:unlock',
   'cleanup',
])->desc('Deploy your project');

// If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
