<?php

class ArticleHolderExtension extends DataExtension {

    private static $db = array(
        'IncludeArticlesOnHomepage' => 'Boolean'
    );

    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldToTab('Root.Main', CheckboxField::create('IncludeArticlesOnHomepage'), 'Content');
    }

}
