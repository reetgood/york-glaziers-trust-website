<?php

class GlossaryPage extends Page {

    /**
     * Database
     */

    private static $has_many = array(
        'Entries' => 'GlossaryEntry'
    );





    /**
     * CMS
     */

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        $fields->removeByName('Content');

        $this->addEntriesField($fields);

        return $fields;
    }

    private function addEntriesField(FieldList $fields) {
        $config = GridFieldConfig_RecordEditor::create();

		$entriesField = new GridField(
			'Entries',
			'Entries',
			$this->Entries(),
			$config
		);

		$fields->addFieldToTab('Root.Entries', $entriesField);
    }
}

class GlossaryPage_Controller extends Page_Controller {

    public function GroupedEntries() {

        $grouped = array();

        foreach ($this->Entries() as $entry) {
            $letter = strtoupper($entry->Title[0]);
            if (!array_key_exists($letter, $grouped)) {
                $grouped[$letter] = array();
            }
            $grouped[$letter][] = $entry;
        }

        // Convert to SS data types.
        $groupedList = new ArrayList();
        foreach ($grouped as $letter => $entries) {
            $groupedList->push(new ArrayData(array(
                'Letter' => $letter,
                'Entries' => new ArrayList($entries)
            )));
        }

        return $groupedList;

    }

}
