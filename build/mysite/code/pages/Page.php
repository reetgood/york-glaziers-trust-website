<?php

class Page extends SiteTree {

    /**
     * Database
     */

    private static $has_one = array(
        'MainImage'     => 'Image',
        'FeaturedImage' => 'Image'
    );

    private static $has_many = array(
        'Galleries' => 'Gallery'
    );

    private static $db = array(
        'MainImageCaption' => 'Text'
    );

    function onBeforeWrite() {
        // Replace <p> around shortcodes with a <div>.
        $this->Content = preg_replace('/<p>(\[.*\])<\/p>/', '<div>$1</div>', $this->Content);
        parent::onBeforeWrite();
    }





    /**
     * CMS
     */

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        $fields->addFieldToTab('Root.Main', ToggleCompositeField::create('MainImageFields', 'Page Images', array(
            UploadField::create('MainImage'),
            $captionField = TextareaField::create('MainImageCaption'),
            $featuredImageField = UploadField::create('FeaturedImage')
        )), 'Content');

        $captionField->setDescription('Leave blank to use the default caption');
        $featuredImageField->setDescription('Image to use on the home page and promo pages.');

        $this->addGalleryField($fields);

        return $fields;
    }

    private function addGalleryField($fields) {
        $config = GridFieldConfig_RecordEditor::create();

        $galleriesField = new GridField(
            'Galleries',
            'Galleries',
            $this->Galleries(),
            $config
        );

        $fields->addFieldToTab('Root.Galleries', $galleriesField);
    }





    /**
     * Data
     */

    private static $casting = array(
        'GalleryShortcode' => 'HTMLText'
    );

    public static function GalleryShortcode($arguments, $content = NULL, $parser = NULL, $tagName) {
        // Load the relevant gallery and render with the gallery template.
        $gallery = Gallery::get()->byID($arguments['id']);
        return $gallery ? $gallery->renderWith('Gallery') : NULL;
    }

    public function MainImageCaptionOrDefault() {
        $caption = $this->MainImageCaption;
        if (!$caption) {
            $config = SiteConfig::current_site_config();
            $caption = $config->Tagline;
        }
        return $caption;
    }

}






class Page_Controller extends ContentController {

    public function init() {
        parent::init();
        Requirements::css("{$this->ThemeDir()}/css/style.min.css");
        Requirements::javascript("{$this->ThemeDir()}/js/main.min.js");
    }

    public function ContactPage() {
        return ContactPage::get()->first();
    }

}
