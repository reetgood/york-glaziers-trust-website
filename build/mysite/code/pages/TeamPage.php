<?php

class TeamPage extends Page {

    /**
     * Database
     */

    private static $has_many = array(
        'TeamGroups' => 'TeamGroup'
    );





    /**
     * CMS
     */

    private static $icon = 'mysite/images/icons/group.png';

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        $this->addTeamGroupsField($fields);

        $fields->removeByName('Content');

        return $fields;
    }

    private function addTeamGroupsField(FieldList $fields) {
        $config = GridFieldConfig_RecordEditor::create();

		$entriesField = new GridField(
			'TeamGroups',
			'Team Groups',
			$this->TeamGroups(),
			$config
		);

		$fields->addFieldToTab('Root.Main', $entriesField);
    }

}

class TeamPage_Controller extends Page_Controller {

}
