<?php

class ContactPage extends Page {

    /**
     * Database
     */

    private static $db = array(
        'Telephone' => 'Varchar(20)',
        'Email'     => 'Varchar(100)',
        'Address1'  => 'Varchar(50)',
        'Address2'  => 'Varchar(50)',
        'TownCity'  => 'Varchar(50)',
        'Postcode'  => 'Varchar(10)',
        'Twitter'   => 'Varchar(100)',
        'Facebook'  => 'Varchar(100)',
        'Instagram' => 'Varchar(100)'
    );





    /**
	 * CMS
	 */

	private static $icon = 'mysite/images/icons/email.png';

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        $fields->addFieldToTab('Root.Main', TextField::create('Telephone'), 'Content');
        $fields->addFieldToTab('Root.Main', EmailField::create('Email'), 'Content');

        $fields->addFieldToTab('Root.Main', TextField::create('Address1'), 'Content');
        $fields->addFieldToTab('Root.Main', TextField::create('Address2'), 'Content');
        $fields->addFieldToTab('Root.Main', TextField::create('TownCity', 'Town / City'), 'Content');
        $fields->addFieldToTab('Root.Main', TextField::create('Postcode'), 'Content');

        $fields->addFieldToTab('Root.Main', TextField::create('Instagram'), 'Content');
        $fields->addFieldToTab('Root.Main', TextField::create('Twitter'), 'Content');
        $fields->addFieldToTab('Root.Main', TextField::create('Facebook'), 'Content');

        return $fields;
    }





    /**
     * Data
     */

    public function getAddress($separator = ', ') {
        $parts = array($this->Address1);

        if ($this->Address2) {
            $parts[] = $this->Address2;
        }

        if ($this->TownCity) {
            $parts[] = $this->TownCity;
        }

        $parts[] = $this->Postcode;
        return implode($separator, $parts);
    }

    public function getTelephoneUrl() {
        return 'tel:' . str_replace(array(' ', '-', '(0)'), '', $this->Telephone);
    }

}

class ContactPage_Controller extends Page_Controller {

}
