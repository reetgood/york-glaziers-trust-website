<?php

class PanelOfTheMonthPage extends ArticlePage {

    /**
     * Database
     */

    public static $has_one = array(
 		'BeforeImage' => 'Image',
 		'AfterImage'  => 'Image'
 	);

    public static $db = array(
        'PanelName' => 'Varchar(250)'
    );





    /**
     * CMS
     */

    public static $icon = 'mysite/images/icons/page-picture.png';

    public function getCMSFields() {
        // Panel of the month needs extra image fields but to remove some
        // of the main image fields for convenience. Same for featured image for
        // the news page, which can also use the 'after' image.
        $fields = parent::getCMSFields();

        $fields->addFieldToTab('Root.Main', Textfield::create('PanelName'), 'Content');
        $fields->addFieldToTab('Root.Main', UploadField::create('BeforeImage'), 'Content');
        $fields->addFieldToTab('Root.Main', UploadField::create('AfterImage'), 'Content');

        // Remove main image fields and featured image.
        $fields->removeByName('MainImageFields');
        $fields->removeByName('FeaturedImage');

        return $fields;
    }





    /**
     * Data
     */

    public function FeaturedImage() {
        return $this->AfterImage();
    }

    public function MainImage() {
        return $this->AfterImage();
    }

    public function MainImageCaption() {
        return $this->PanelName;
    }

}

class PanelOfTheMonthPage_Controller extends ArticlePage_Controller {

}
