<?php

class HomePage extends Page {

    /**
     * Database
     */

    private static $has_one = array(
        'TeamImage'        => 'Image',
        'FeaturedProduct'  => 'ProductPage',
        'PublicationsPage' => 'Page'
    );

    private static $many_many = array(
        'MainImages' => 'Image',
        'PromoPages' => 'Page'
    );

    private static $many_many_extraFields = array(
        'MainImages' => array(
            'SortOrder' => 'Int'
        ),
        'PromoPages' => array(
            'SortOrder' => 'Int'
        )
    );

    private static $db = array(
        'WorkTitle'            => 'Varchar(50)',
        'WorkContent'          => 'HTMLText',
        'FeaturedProductTitle' => 'Varchar(50)',
        'TeamTitle'            => 'Varchar(50)',
        'TeamContent'          => 'HTMLText'
    );





    /**
     * CMS
     */

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        // Replace single featured image with many.
        $fields->removeByName('MainImageFields');
        $fields->addFieldToTab('Root.Main', SortableUploadField::create('MainImages', 'Main Images'), 'Content');

        $workFields = ToggleCompositeField::create('WorkFields', 'Our Work', array(
            TextField::create('WorkTitle', 'Title'),
            HTMLEditorField::create('WorkContent', 'Metadata')->setRows(10)
        ));
        $fields->addFieldToTab('Root.Main', $workFields, 'Metadata');

        $productOptions = ProductPage::get()->map('ID', 'Title');
        $productFields = ToggleCompositeField::create('FeaturedProductFields', 'Featured Product', array(
            TextField::create('FeaturedProductTitle', 'Title'),
            DropdownField::create('FeaturedProductID', 'Product', $productOptions)->setEmptyString('(None)')
        ));
        $fields->addFieldToTab('Root.Main', $productFields, 'Metadata');

        $teamFields = ToggleCompositeField::create('TeamFields', 'Team', array(
            TextField::create('TeamTitle', 'Title'),
            HTMLEditorField::create('TeamContent', 'Metadata')->setRows(10),
            UploadField::create('TeamImage', 'Image')
        ));
        $fields->addFieldToTab('Root.Main', $teamFields, 'Metadata');

        $fields->addFieldToTab('Root.Main', TreeDropdownField::create('PublicationsPageID', 'Publications Page', 'Page'), 'Metadata');

        $this->addPromosField($fields);

        return $fields;
    }

    private function addPromosField($fields) {
        $config = GridFieldConfig_RelationEditor::create();
        $config->addComponent(new GridFieldSortableRows('SortOrder'));

        // We don't want to be able to add or edit pages, just link them.
        $config->removeComponentsByType('GridFieldAddNewButton');
        $config->removeComponentsByType('GridFieldEditButton');

        $promosField = new GridField(
            'PromoPages',
            'Promo Pages',
            $this->PromoPages(),
            $config
        );

        $fields->addFieldToTab('Root.Promos', $promosField);
    }





    /**
     * Data
     */

    public function SortedMainImages() {
        return $this->MainImages()->Sort('SortOrder');
    }

    public function SortedPromoPages() {
        return $this->PromoPages()->Sort('SortOrder');
    }

}

class HomePage_Controller extends Page_Controller {

    public function LatestArticlePage() {
        $articleHolder = ArticleHolder::get()->where('IncludeArticlesOnHomepage', true)->First();
        return $articleHolder ? $articleHolder->RecentArticles(1)->First() : null;
    }

    public function TeamPage() {
        return TeamPage::get()->first();
    }

    public function PublicationsContent() {

        // Limit to only first two <p> tags by loading the content into a
        // DOMDocument then getting all paragraphs and concatenating their HTML.
        $content = $this->PublicationsPage()->Content;
        $dom = new DOMDocument;
        $dom->loadHTML($content);
        $paragraphs = $dom->getElementsByTagName('p');

        $html = '';

        if ($paragraph1 = $paragraphs->item(0)) {
            $html .= $dom->saveHTML($paragraph1);
            if ($paragraph2 = $paragraphs->item(1)) {
                $html .= $dom->saveHTML($paragraph2);
            }
        }

        return $html;
    }

}
