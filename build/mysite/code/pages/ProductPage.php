<?php

class ProductPage extends Page {

    /**
     * Database
     */

    private static $has_one = array(
        'MainImage' => 'Image'
    );

    private static $has_many = array(
        'LookInsideImages' => 'LookInsideImage'
    );

    private static $db = array(
        'PayPalButtonID' => 'Varchar(50)',
        'Price'          => 'Currency'
    );





    /**
     * CMS
     */

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        // Add product fields.
        $fields->addFieldToTab('Root.Main', UploadField::create('MainImage'), 'Content');
        $fields->addFieldToTab('Root.Main', CurrencyField::create('Price'), 'Content');

        $fields->addFieldToTab('Root.Main', $buttonIdField = TextField::create('PayPalButtonID', 'PayPal Button ID'), 'Content');
        $buttonIdField->setDescription('ID copied from the PayPal pay button page.');

        // Look inside images
        $config = GridFieldConfig_RecordEditor::create();
        $config->addComponent(new GridFieldSortableRows('SortOrder'));
        $lookInsideImagesField = GridField::create(
            'LookInsideImages',
            'Look Inside Images',
            $this->LookInsideImages(),
            $config
        );
        $fields->addFieldToTab('Root.Main', $lookInsideImagesField);

        return $fields;
    }

}
