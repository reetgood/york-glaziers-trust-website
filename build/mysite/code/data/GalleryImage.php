<?php

class GalleryImage extends DataObject {

    /**
     * Database
     */

    private static $has_one = array(
        'Gallery'    => 'Gallery',
        'Image'      => 'Image',
        'LargeImage' => 'Image'
    );

    private static $db = array(
        'SortOrder' => 'Int'
    );

    private static $default_sort = 'SortOrder ASC';





    /**
     * CMS
     */

    private static $summary_fields = array(
        'Image.CMSThumbnail'      => 'Image',
        'LargeImage.CMSThumbnail' => 'Large Image'
    );

     public function getCMSFields() {
        $fields = parent::getCMSFields();

        // Remove auto-set fields.
        $fields->removeByName('GalleryID');
        $fields->removeByName('SortOrder');

        return $fields;
    }

    public function canCreate($member = NULL) { return TRUE; }
    public function canEdit($member = NULL)   { return TRUE; }
    public function canDelete($member = NULL) { return TRUE; }
    public function canView($member = NULL)   { return TRUE; }

}
