<?php

class Gallery extends DataObject {

    /**
     * Database
     */

    private static $has_one = array(
        'Page' => 'Page'
    );

    private static $has_many = array(
        'Images' => 'GalleryImage'
    );

    private static $db = array(
        'Caption'   => 'Text'
    );





    /**
     * CMS
     */

    private static $summary_fields = array(
        'ID', 'Caption'
    );

     public function getCMSFields() {
        $fields = parent::getCMSFields();

        // Remove auto-set fields.
        $fields->removeByName('PageID');
        $fields->removeByName('SortOrder');

        // Add help text.
        if ($this->isInDB()) {
            // After saving, output sample shortcode.
            $helpText = 'To use this gallery, paste this shortcode into the content: [gallery id=' . $this->ID . ']';
        } else {
            // Before saving, explain it needs to be saved.
            $helpText = 'To add images and use this gallery, please first save it.';
        }
        $fields->addFieldToTab('Root.Main', LiteralField::create('HelpText', '<p class="message notice">' . $helpText . '</p>'), 'Caption');

        // Repace caption field with a textarea.
        $fields->replaceField('Caption', TextareaField::create('Caption'));

        // Update grid field config to be more like record editor and have reordering.
        $imagesField = $fields->dataFieldByName('Images');
        if ($imagesField) {
            $config = $imagesField->getConfig();
            $config->addComponent(new GridFieldSortableRows('SortOrder'));
            $config->removeComponentsByType('GridFieldAddExistingAutocompleter');
            $config->removeComponentsByType('GridFieldDeleteAction');
            $config->addComponent(new GridFieldDeleteAction());
        }

        return $fields;
    }

    public function canCreate($member = NULL) { return TRUE; }
    public function canEdit($member = NULL)   { return TRUE; }
    public function canDelete($member = NULL) { return TRUE; }
    public function canView($member = NULL)   { return TRUE; }





    /**
     * Data
     */

    public function getTitle() {
        return $this->Caption;
    }
}
