<?php

class LookInsideImage extends DataObject {

    /**
     * Database
     */

    private static $has_one = array(
        'Page'     => 'ProductPage',
        'Image'    => 'Image'
    );

    private static $db = array(
        'Details'   => 'Varchar(250)',
        'SortOrder' => 'Int'
    );

    private static $default_sort = 'SortOrder ASC';





    /**
     * CMS
     */

    private static $summary_fields = array(
        'Image.CMSThumbnail' => 'Image',
        'Details'            => 'Details'
    );

     public function getCMSFields() {
        $fields = parent::getCMSFields();

        // Remove auto-set fields.
        $fields->removeByName('PageID');
        $fields->removeByName('SortOrder');

        // Repace details field with a textarea.
        $fields->replaceField('Details', TextareaField::create('Details'));

        return $fields;
 	}

    public function canCreate($member = NULL) { return TRUE; }
    public function canEdit($member = NULL)   { return TRUE; }
    public function canDelete($member = NULL) { return TRUE; }
    public function canView($member = NULL)   { return TRUE; }

}
