<?php

class TeamGroup extends DataObject {

    /**
     * Database
     */

    private static $has_many = array(
        'TeamMembers' => 'TeamMember'
    );

    private static $has_one = array(
        'TeamPage' => 'TeamPage'
    );

    private static $db = array(
        'Title' => 'Varchar(25)'
    );





    /**
     * CMS
     */

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        // Remove auto-set relationship fields.
        $fields->removeByName('TeamPageID');

        // TODO: Move team members grid field from its own tab into main.
        // This was done on another project, just need to find it.

        // Reconfigure team members field to be more like a record editor.
        $membersField = $fields->dataFieldByName('TeamMembers');
        if ($membersField) {
            $config = $membersField->getConfig();
            $config->addComponent(new GridFieldSortableRows('SortOrder'));
            $config->removeComponentsByType('GridFieldAddExistingAutocompleter');
            $config->removeComponentsByType('GridFieldDeleteAction');
            $config->addComponent(new GridFieldDeleteAction());
        }

        return $fields;
    }

    public function canCreate($member = NULL) { return TRUE; }
 	public function canEdit($member = NULL)   { return TRUE; }
 	public function canDelete($member = NULL) { return TRUE; }
 	public function canView($member = NULL)   { return TRUE; }

}
