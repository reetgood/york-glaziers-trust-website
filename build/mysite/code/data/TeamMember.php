<?php

class TeamMember extends DataObject {

    /**
     * Database
     */

    private static $has_one = array(
        'TeamGroup' => 'TeamGroup',
        'Image'     => 'Image'
    );

    private static $db = array(
        'Name'      => 'Varchar(50)',
        'Honours'   => 'Varchar(100)',
        'Role'      => 'Varchar(100)',
        'Details'   => 'HTMLText',
        'SortOrder' => 'Int'
    );

    private static $default_sort = 'SortOrder ASC';




    /**
     * CMS
     */

    public function getCMSFields() {
        $fields = parent::getCMSFields();

        // Reduce size of details field.
        $fields->dataFieldByName('Details')->setRows(10);

        // Remove auto-set fields.
        $fields->removeByName('TeamGroupID');
        $fields->removeByName('SortOrder');

        return $fields;
    }

    public function canCreate($member = NULL) { return TRUE; }
 	public function canEdit($member = NULL)   { return TRUE; }
 	public function canDelete($member = NULL) { return TRUE; }
 	public function canView($member = NULL)   { return TRUE; }

}
