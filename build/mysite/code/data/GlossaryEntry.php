<?php

class GlossaryEntry extends DataObject {

    /**
     * Database
     */

    private static $has_one = array(
        'Glossary' => 'GlossaryPage',
        'Image'    => 'Image'
    );

    private static $db = array(
        'Title'   => 'Varchar(30)',
        'Details' => 'Text'
    );

    private static $default_sort = 'Title ASC';





    /**
     * CMS
     */

     public function getCMSFields() {
 		$fields = parent::getCMSFields();

 		// Remove glossary page link field.
 		$fields->removeByName('GlossaryID');

        // Repace details field with a textarea.
        $fields->replaceField('Details', TextareaField::create('Details'));

 		return $fields;
 	}

 	public function canCreate($member = NULL) { return TRUE; }
 	public function canEdit($member = NULL)   { return TRUE; }
 	public function canDelete($member = NULL) { return TRUE; }
 	public function canView($member = NULL)   { return TRUE; }

}
