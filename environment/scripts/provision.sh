apt-get update

apt-get install -y unzip

# Apache
apt-get install -y apache2

# Configure
a2enmod rewrite
systemctl restart apache2

# Symlink Apache www directory to vagrant build
rm -rf /var/www/html
ln -s /vagrant/build /var/www/html

# Copy site config.
cp /vagrant/environment/config/000-default.conf /etc/apache2/sites-available

echo "Apache installed"

# MySQL
apt-get install -y mysql-server
mysql -u root -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'ss';"
echo "MySQL installed"

# PHP
add-apt-repository ppa:ondrej/php
apt-get install -y php5.6 php5.6-mcrypt php5.6-mbstring php5.6-curl php5.6-cli php5.6-mysql php5.6-gd php5.6-intl php5.6-xsl php5.6-zip libapache2-mod-php5.6

# Change owner of PHP session to match Apache user.
if [ -d /var/lib/php/sessions ]
then
	chown -R vagrant:vagrant /var/lib/php/sessions
fi

# Install Composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php --install-dir=/usr/bin --filename=composer --version=1.10.19
php -r "unlink('composer-setup.php');"

echo "PHP installed"