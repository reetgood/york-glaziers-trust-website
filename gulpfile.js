// Gulp
const gulp = require('gulp');

// Requires
const express = require('express');
const open = require('open');
const rimraf = require('rimraf');
const stylish = require('jshint-stylish');

// Plugins
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const gulpif = require('gulp-if');
const jshint = require('gulp-jshint');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const sassLint = require('gulp-sass-lint');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
const newer = require('gulp-newer');
const gulpLivereload = require('gulp-livereload');
const insert = require('gulp-insert');

// Constants
var LIVERELOAD_PORT = 35729;

// Load external config.
const config = require('./gulp-config.json');
const themeDirectory = config.themeDirectory;

// Load command-line arguments.
const argv = require('yargs').argv;
const lint = argv.lint !== undefined ? argv.lint : true;

/**
 * Script build task. Combines and uglifies JS, producing
 * both a minified and non-minified version in dist/ and
 * dev/ respectively.
 *
 * 1. Using all files defined in files.scripts config.
 * 2. Run JSHint and report the output.
 * 3. Combine into main.js
 * 4. Output development version.
 * 5. Rename to main.min.js
 * 6. Uglify to minify.
 * 7. Output minified version.
 */
const scripts = () => {
  const outputDirectory = themeDirectory + 'js/';

  return gulp
    .src(config.files.scripts) /* [1] */
    .pipe(gulpif(lint, jshint())) /* [2] */
    .pipe(gulpif(lint, jshint.reporter(stylish)))
    .pipe(concat('main.js')) /* [3] */
    .pipe(gulp.dest(outputDirectory)) /* [4] */
    .pipe(rename({ suffix: '.min' })) /* [5] */
    .pipe(uglify()) /* [6] */
    .pipe(gulp.dest(outputDirectory)); /* [7] */
};

/**
 * Styles build task. Compiles CSS from SASS, auto-prefixes
 * and outputs both a minified and non-minified version into
 * dist/ and dev/ respectively.
 *
 * 1. Using all files defined in files.styles config.
 * 2. Compile using SASS, expanded style.
 * 3. Auto-prefix (e.g. -moz-) using last 2 browser versions.
 * 4. Output prefixed but non-minifed CSS to dev/css
 * 5. Rename to .min.css
 * 6. Minify the CSS.
 * 7. Output prefixed, minified CSS to dist/css.
 */
const styles = () => {
  const outputDirectory = themeDirectory + 'css/';
  return gulp
    .src(config.files.styles) /* [1] */
    .pipe(gulpif(lint, sassLint({ config: '.sass-lint.yml' })))
    .pipe(gulpif(lint, sassLint.format()))
    .pipe(sass({ style: 'expanded' }))
    .pipe(autoprefixer('last 2 versions')) /* [3] */
    .pipe(gulp.dest(outputDirectory)) /* [4] */
    .pipe(rename({ suffix: '.min' })) /* [5] */
    .pipe(cleanCSS()) /* [6] */
    .pipe(gulp.dest(outputDirectory)); /* [7] */
};

/**
 * Image optimsation task.
 *
 * 1. Determine whether to use imagemin or do nothing (noop).
 * 2. Use files defined in files.images config.
 * 3. Filter to only images that are newer than in the destination.
 * 4. Output optimised images.
 */
const images = () => {
  const outputDirectory = themeDirectory + 'images/';

  return gulp
    .src(config.files.images) /* [2] */
    .pipe(newer(outputDirectory)) /* [3] */
    .pipe(
      gulpif(
        config.minifyImages,
        imagemin([
          imagemin.gifsicle({ interlaced: true }),
          imagemin.mozjpeg({ progressive: true }),
          imagemin.optipng({ optimizationLevel: 5 }),
          imagemin.svgo({
            plugins: [{ removeViewBox: false }]
          })
        ])
      )
    )
    .pipe(gulp.dest(outputDirectory)); /* [4] */
};

/**
 * Clean task. Deletes generated files.
 */
const clean = callback => {
  // TODO reduce chaining if possible.
  return rimraf(themeDirectory + 'css/', function () {
    rimraf(themeDirectory + 'js/', function () {
      rimraf(themeDirectory + 'images/', callback);
    });
  });
};

/**
 * Watch task. Sets up several watchers. Using different config for styles and
 * templates as they have partials that need watching but not compiling.
 *
 * 1. Any changes to any files from files.watchStyles config starts styles task.
 * 2. Any changes to any files from files.scripts config starts scripts task.
 * 3. Any changes to any files from files.images config starts images task.
 */
const watch = () => {
  gulp.watch(config.files.watchStyles, gulp.series(styles)); /* [1] */
  gulp.watch(config.files.scripts, gulp.series(scripts)); /* [2] */
  gulp.watch(config.files.images, images); /* [3] */
};
/**
 * LiveReload task.
 *
 * 3. Inform the LiveReload server of any change in the theme directory.
 */
const livereload = () => {
  gulpLivereload.listen(LIVERELOAD_PORT);
  gulp.watch(themeDirectory + '**/*').on('change', gulpLivereload.changed); // [3]
};

/**
 *    Build task. Runs other tasks that produce a built project
 *    in /dev and /dist.
 *
 * 1. Using runsequence to run clean first, separate to the build tasks. Passing
 *    the Gulp callback to runsequence so that the task can complete correctly.
 */
const build = gulp.series(
  clean,
  gulp.parallel(images, styles, scripts)
);

/**
 * Develop task.
 */
const develop = gulp.series(build, gulp.parallel(watch, livereload));

exports.default = develop;
exports.build = build;
exports.develop = develop;
