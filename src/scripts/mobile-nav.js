/* global jQuery */
jQuery(function($) {

	'use strict';

	var nav = $('.c-page-head__nav');

	var trigger = jQuery('<button class="c-btn c-btn--full c-btn--small c-page-head__nav-trigger">☰ Menu</button>');
	nav.before(trigger);

	trigger.on('click', function() {
		nav.toggleClass('is-expanded');
	});

});
