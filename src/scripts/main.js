jQuery('html').removeClass('no-js').addClass('js');

// Fix dropdown navigation on touch devices that don't have the mobile nav.
if ($(window).width() >= 1024) {
	jQuery('.c-page-head__nav li:has(ul)').doubleTapToGo();
}
