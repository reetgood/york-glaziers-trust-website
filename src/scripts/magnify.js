/* global jQuery */
jQuery(function($) {
	'use strict';

	$('.js-magnifier').each(function() {

		var originalImageWidth,
			originalImageHeight,
			imageContainer,
			magnifier;

		var el = $(this);
		var largeImage = el.data('magnified');

		// Build the wrapper.
		magnifier = $('<div>').addClass('c-magnifier__large').css('background-image', 'url(' + largeImage + ')');
		imageContainer = $('<div class="c-magnifier">');
		el.wrap(imageContainer);
		el.addClass('c-magnifier__img');
		el.after(magnifier);

		// Load the large image.
		var img = new Image();
		img.src = largeImage;
		img.onload = function() {

			originalImageWidth = img.width;
			originalImageHeight = img.height;

			el.parent().on('mousemove', function(ev) {

				var $this = $(this);
				var magnifyOffset = $this.offset();
				var mx = ev.pageX - magnifyOffset.left;
				var my = ev.pageY - magnifyOffset.top;

				if (mx > 0 && my > 0 && mx < $this.width() && my < $this.height()) {
					magnifier.fadeIn(100);
				} else {
					magnifier.fadeOut(100);
				}
				if (magnifier.is(':visible')) {

					var rx = Math.round(mx / el.width() * originalImageWidth - magnifier.width() / 2) * -1;
					var ry = Math.round(my / el.height() * originalImageHeight - magnifier.height() / 2) * -1;
					var bgp = rx + 'px ' + ry + 'px';
					var px = mx - magnifier.width() / 2;
					var py = my - magnifier.height() / 2;

					magnifier.css({ left: px, top: py, backgroundPosition: bgp });
				}
			});
		};


	});

});
