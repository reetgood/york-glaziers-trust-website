/* global jQuery */
jQuery(function($) {
	'use strict';

	var tabs = $('.c-tabs__tab'),
	    tabPanels = $('.c-tabs__panel');

	tabs.on('click', function() {
		var tab = $(this),
        	tabPanelId = tab.attr('aria-controls'),
        	tabPanel = $('#' + tabPanelId);

    	// De-select all and show current.
    	tabs.attr('aria-selected', 'false').removeClass('is-active');
		tab.attr('aria-selected', 'true').addClass('is-active');

    	// Hide all tab panels and show current.
    	tabPanels.attr('aria-hidden', 'true').addClass('is-hidden');
		tabPanel.attr('aria-hidden', 'false').removeClass('is-hidden');
	});
});
