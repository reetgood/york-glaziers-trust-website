/* global jQuery */
jQuery(function($) {
	'use strict';

	var fadeDelay = 4000;
	var fadeDuration = 1000;
	var minSize = 1024;

	// Stop here on small screens.
	if ($(window).width() < minSize) {
		return;
	}

	$('.js-fader').each(function() {
		var fader = $(this),
			images = fader.find('img'),
			total = images.length,
			current = 0;

		// Set src attribute from data-src
		fader.find('[data-src]').each(function() {
			var img = $(this);
			img.hide();
			img.attr('src', img.attr('data-src')).removeAttr('data-src');
		});

		function next() {

			images.eq(current).css('zIndex', 1).fadeOut(fadeDuration);

			if (current + 1 < total) {
				current++;
			} else {
				current = 0;
			}

			images.eq(current).css('zIndex', 0).fadeIn(fadeDuration);
		}

		window.setInterval(next, fadeDelay);
	});
});
